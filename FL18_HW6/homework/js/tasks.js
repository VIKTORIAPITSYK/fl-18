function getMaxEvenElement(arr) {
  let two = 2;
  return Math.max(...arr.filter(x => x%two!==0));
}

let a = 3;
let b = 5;
[a, b] = [b, a];

function getValue(val) {
  return val ?? '-'
}

function objFromArr(arr) {
  let result = {};
  arr.forEach(x => {
    result[x[0]]=x[1]
  });
  return result
}

function getRegroupedObj(obj) {
  let newObject = {};
  let {name: firstName, details} = obj;
  let {id, age, university} = details;
  newObject.university = university;
  newObject.user = {age, firstName, id};
  return newObject
}

function getArrayWithUniqueElements(arr) {
 return new Set(arr)
}

function hideNumber(str) {
  return str.slice(-4).padStart(str.length, '*')
}

function isRequare() {
  throw 'Some parameters are missing'
}
function add(a=isRequare(), b=isRequare()) {
  return a+b
}

function* generateIterableSequence(arr = ['I', 'Love', 'EPAM']) {
  for (let word of arr) {
    yield word
  }
}
const generatorObj = generateIterableSequence();
for (let value of generatorObj) {
  console.log(value);
}
