import { playGame } from './PlayGame.js';
import { winner } from './Winner.js';
import "../scss/first.scss";

window.addEventListener('load', function() {
  let countUser = document.querySelector('.count-user'),
      countComp = document.querySelector('.count-comp'),
      userField = document.querySelector('.user-field'),
      compField = document.querySelector('.comp-field'),
      fields = document.querySelectorAll('.field'),
      play = document.querySelector('.play'),
      result = document.querySelector('.result'),
      userStep, compStep, countU = 0, countC = 0, blocked = false;

  function choiseUser(e) {
    if (blocked) return;
    let target = e.target;
    if(target.classList.contains('field')) {
      userStep = target.dataset.field;
      fields.forEach(item => item.classList.remove('active', 'error'))
      target.classList.add('active');
      choiseComp();
    }
  };

  function choiseComp() {
      blocked = true;
      let rand = Math.floor(Math.random() * 3);
      compField.classList.add('blink');
      let compFields = compField.querySelectorAll('.field');
      setTimeout(() => {
        compField.classList.remove('blink');
        compStep = compFields[rand].dataset.field;
        console.log(compStep);
        compFields[rand].classList.add('active')
        winner();
      }, 3000);
  };

  play.addEventListener('click', playGame);
  userField.addEventListener('click', choiseUser);
});
