import '../css/style.css';
import { clearField } from './ClearField.js';
import { randomMove } from './RandomMove.js';
import { checkWinner } from './CheckWinner.js'

let won = document.querySelector('.won');
let loose = document.querySelector('.loose');
let draw = document.querySelector('.draw');
let gameField = document.getElementById('gameField');
let btn_clear = document.getElementById('btn_clear');
let btn_newGame = document.getElementById('btn_newGame');
let allBlocks = document.getElementsByClassName('block');
let playerScore = document.getElementById('playerScore');
let compScore = document.getElementById('compScore');
let playerCount = 0;
let compCount = 0;
let counter = 0;
let turnPlayer = Math.round(Math.random());

btn_clear.onclick = function() {
  clearField();
  turnPlayer = Math.round(Math.random());
  counter = 0;
}
btn_newGame.onclick = function() {
  clearField();
  playerCount = compCount = counter = 0;
  playerScore.innerText = '0';
  compScore.innerText = '0';
};

for (let i=0; i<9; i++) {
  gameField.innerHTML += '<div class="block"></div>';
}

if (turnPlayer === 0) {
  randomMove();
}

gameField.onclick = function(event) {
  if (event.target.innerText !== '') {
    return
  } else {
    event.target.innerText = 'X'
    counter++;
    console.log(counter)
  }
  checkWinner('X');
  if (checkFreeSpace()) {
    randomMove();
  }
  checkWinner('0');

}

function checkFreeSpace() {
  for (let i=0; i<allBlocks.length; i++) {
    if (allBlocks[i].innerText==='') {
      return true
    };
  }
}
