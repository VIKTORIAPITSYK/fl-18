const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    main: path.resolve(__dirname, './js/index.js'),
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js',
  },
  module: {
      rules: [{
         test:/\.(s*)css$/,
         use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
         ]
      }]
   },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'webpack Boilerplate',
      template: path.resolve(__dirname, './index.html'), // шаблон
      filename: 'ind.html', // название выходного файла
    }),
    new MiniCssExtractPlugin({
         filename: 'main.css',
      }),
  ],
}
