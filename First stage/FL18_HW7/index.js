function getAge(birthday) {
  let today = new Date();
  return today.getFullYear() - birthday.getFullYear()
}

function getWeekDay(date) {
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  if (typeof date === 'number') {
    date = new Date(date)
  }
  let day = date.getDay();
  return days[day]
}

function getAmountDaysToNewYear() {
  let today = new Date();
  let newYear = new Date(today.getFullYear(), 0, 1);
  if (today.getMonth()===0 && today.getDate()>1) {
    newYear.setFullYear(newYear.getFullYear()+1);
  }
  let oneDay=1000*60*60*24;
  let daysLeft = 365 - Math.abs(Math.ceil((newYear.getTime()-today.getTime())/oneDay));
  return daysLeft
}

function getProgrammersDay(year) {
  const resultOne = `${year} (${getWeekDay(new Date(year, 8, 13))})`;
  const resultTwo = `${year} (${getWeekDay(new Date(year, 8, 12))})`;
  return year%4 === 0 ? `12 Sep, ${resultOne}`: `13 Sep, ${resultTwo}`
}

function howFarIs(day) {
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  let today = new Date().getDay();
  day = day.charAt(0).toUpperCase() + day.slice(1)
  let amount = Math.abs(days.indexOf(day)-today) - 1;
  if (amount === 0) {
    return `Today is ${day}!`
  } else {
    return `It's ${amount} day(s) left till ${day}.`
  }
}

function isValidIdentifier(vari) {
  return /^[a-zA-Z_$][a-zA-Z_$0-9]*$/.test(vari)
}

function capitalize(testStr) {
  return testStr.replace(/\b[a-z]*\b/gi, x => x.charAt(0).toUpperCase()+x.slice(1))
}

function isValidAudioFile(file) {
  let reg = /^[a-zA-Z]+\.mp3|flac|alac|aac$/;
  return reg.test(file)
}

function getHexadecimalColors(pass) {
  let re = /#([0-9a-f]{3}){1,2}\b/gi;
  return pass.match(re)
}


function isValidPassword(pass) {
  let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/g
  return re.test(pass)
}

function addThousandsSeparators(x) {
  x = x.toString();
  let pattern = /(-?\d+)(\d{3})/;
  while (pattern.test(x)) {
  x = x.replace(pattern, '$1,$2');
  }
  return x
}

function getAllUrlsFromText(text) {
  try {
    let re = /(https?:\/\/[^\s]+)/g;
    let result = text.match(re);
    return result || []
  } catch (err) {
    return 'Errorrrr'
  }
}
