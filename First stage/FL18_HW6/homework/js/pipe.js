function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...funcs) => {
  try {
    funcs.forEach((funct, i) => {
      if (!isFunction(funct)) {
        throw new Error(`Provided argument at position ${i} is not a function!`)
      }
      value = funct(value);
    })
  } catch (err) {
    return err;
  }
  return value
};
