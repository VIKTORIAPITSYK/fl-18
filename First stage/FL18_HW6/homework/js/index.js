let i = 1;
function visitLink(path) {
  let i = localStorage.getItem(path);
  i++
  localStorage.setItem(path, i);
}

function viewResults() {
  let obj = {
    'Page1': localStorage.getItem('Page1'),
    'Page2': localStorage.getItem('Page2'),
    'Page3': localStorage.getItem('Page3')
  }
  let ul = document.createElement('ul');
  for (let key in obj) {
      let li = document.createElement('li');
      if (obj[key]===null) {
        obj[key]=0;
    }
    li.textContent = `You visited ${key} ${obj[key]} time(s)`;
    ul.append(li);
  }
  document.querySelector('.container').append(ul);
  localStorage.clear();
}
