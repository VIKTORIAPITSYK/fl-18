
function reverseNumber(num) {
  let result = '';
  let str = num.toString();
  let minus = num>0 ? 0 : 1;
  for (let i=str.length-1; i>=minus; i--) {
    result += str[i];
  }
  if (num>0) {
    result = +result
  } else {
    result = +('-' + result)
  }
  return result
}

function forEach(arr, func) {
  for (let i=0; i<arr.length; i++) {
    arr[i]=func(arr[i])
  }
}

function map(arr, func) {
  let result = [];
  forEach(arr, function(elem){
    result.push(func(elem))
  });
  return result
}

function filter(arr, func) {
  let result = [];
  forEach(arr, function(value){
    if(func(value)) {
      result.push(value)
  }
});
  return result
}

function getAdultAppleLovers(data) {
let adult = 17;
let result = filter(data, x => x.age>adult && x.favoriteFruit==='apple');
let names = map(result, x => x.name);
  return names
}

function getKeys(obj) {
  let result = [];
    for (let key in obj) {
      if(key) {
        result.push(key)
      }
    }
  return result
}

function getValues(obj) {
  let result = [];
    for (let key in obj) {
      if(key){
      result.push(obj[key])        
      }
    }
    return result
}

function showFormattedDate(dateObj) {
  const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December'];
  let numberThree = 3;
  let month = monthNames[dateObj.getMonth()].slice(0, numberThree);
  let day = dateObj.getDate();
  let year = dateObj.getFullYear();
  return `This is ${day} of ${month}, ${year}`
}
