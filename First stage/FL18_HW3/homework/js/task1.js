/*eslint-disable no-self-compare */
/*eslint-disable no-magic-numbers*/
let amount = +prompt('Amount of money?');
let years = +prompt('How many years?');
let percentage = +prompt('How many percent?');
if (amount !== amount || years !== years || percentage !== percentage
  || amount<1000 || years<1 || percentage>100
  || !Number.isInteger(years)) {
  alert('Invalid input data');
} else {
  let totalAmount = amount;
  for (let i=0; i<years; i++) {
    let profit = totalAmount/100*percentage;
    totalAmount += profit;
  }
  let totalProfit = (totalAmount-amount).toFixed(2);
  totalAmount = totalAmount.toFixed(2);
  alert(`Initial amount: ${amount}
  Number of years: ${years}
  Percentage of year: ${percentage}
  Total profit: ${totalProfit}
  Total amount: ${totalAmount}`)
}
