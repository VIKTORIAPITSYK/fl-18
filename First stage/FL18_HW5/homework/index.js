function isEquals(a, b) {
  return a === b
}

function isBigger(a, b) {
  return a > b
}

function storeNames(...arr) {
  return arr
}

function getDifference(a, b) {
  return a > b ? a - b : b - a
}

function negativeCount(arr) {
  return arr.filter(x => x<0).length
}

function letterCount(word, letter) {
  return word.split(letter).length - 1
}

function countPoints(arr) {
  let counter = 0;
  let score = 3;
  arr.forEach(x => {
    x = x.split(':');
    if (+x[0] > +x[1]){
      counter += score;
    } else if (+x[0] === +x[1]) {
      counter += 1;
    }
  })
  return counter
}
