const appRoot = document.getElementById('app-root');

let h1 = document.createElement('h1');
h1.innerText = 'Countries Search';
appRoot.append(h1);

let form = document.createElement('form');
appRoot.append(form);
let firstField = document.createElement('div');
firstField.innerHTML = `
<div class='flex'>
<p>Please choose the type of search:</p>
<div>
<div>
<input type='radio' name='typeOfSearch' value='region' id='inputRegion' onclick='showSelect()'>
<label for='inputRegion'>By Region</label>
</div>
<div>
<input name='typeOfSearch' type='radio' value='language' id='inputLanguage' onclick='showSelect()'>
<label for='inputLanguage'>By Language</label>
</div>
</div>
</div>`
form.append(firstField);

let secondField = document.createElement('div');
secondField.innerHTML = `
<div class='flex'>
<p>Please choose search query:</p>
<select id='select' onclick='selectOption()' disabled>
<option selected>Select value</option>
</select>
</div>
`
form.append(secondField)

let tableField = document.createElement('div');
tableField.innerHTML = `
<div>
<p>No items, please choose search query</p>
</div>
`
appRoot.append(tableField)

function showSelect() {
  let select = document.getElementById('select');
  select.disabled = false;
  select.innerHTML='<option selected>Select value</option>';
  let value = event.target.value;
  (function(value) {
    let arrOfOptions;
    if (value === 'region') {
      arrOfOptions = externalService.getRegionsList();
      select.name = 'region'
    } else if (value === 'language') {
      arrOfOptions = externalService.getLanguagesList();
      select.name = 'language'
    }
    arrOfOptions.forEach(x => {
      let option = document.createElement('option');
      option.innerText = `${x}`;
      select.append(option);
    })
  })(value)
}

function selectOption() {
  let value = event.target.value;
  let listOfCountries;
  if (value !== 'Select value') {
    if (select.name === 'region') {
      listOfCountries = externalService.getCountryListByRegion(value);
      console.log(listOfCountries);
    } else if (select.name === 'language'){
      listOfCountries = externalService.getCountryListByLanguage(value);
    }
    tableField.innerHTML = `
    <table>
    <thead>
    <tr>
    <td>Country name<span onclick='sortByName(0)'>⬍</span></td>
    <td>Capital</td>
    <td>World region</td>
    <td>Languages</td>
    <td>Area<span onclick = 'sortByArea(4)'>⬍</span></td>
    <td>Flag</td>
    </tr>
    </thead>
    <tbody>
    </tbody>
    </table>
    `
    let tbody = document.querySelector('tbody');
    for (let value of listOfCountries) {
      let tr = document.createElement('tr');
      tr.innerHTML = `<td>${value.name}</td><td>${value.capital}</td><td>${value.region}</td>
      <td>${Object.values(value.languages)}</td><td>${value.area}</td><td><img src='${value.flagURL}' alt =''</td>`;
      tbody.innerHTML += tr.outerHTML;
    }
  }
}
function changeArrow(dir, i) {
  let arrow = document.querySelectorAll('td span')[i];
  if (dir === 'down') {
    arrow.textContent = '🠗'
  } else if (dir === 'up'){
    arrow.textContent = '🠕'
  }
}

function sortByName(n) {
  let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.querySelector('table');
  switching = true;
  dir = 'asc';
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i<rows.length - 1; i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName('TD')[n];
      y = rows[i + 1].getElementsByTagName('TD')[n];
      if (dir === 'asc') {
        changeArrow('down', 0);
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      } else if (dir === 'desc') {
        changeArrow('up', 0);
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;
    } else {
      if (switchcount === 0 && dir === 'asc') {
        dir = 'desc';
        switching = true;
      }
    }
  }
}

function sortByArea(n) {
  let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.querySelector('table');
  switching = true;
  dir = 'asc';
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i<rows.length - 1; i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName('TD')[n];
      y = rows[i + 1].getElementsByTagName('TD')[n];
      if (dir === 'asc') {
        changeArrow('down', 1);
        if (+x.innerHTML > +y.innerHTML) {
          shouldSwitch = true;
          break;
        }
      } else if (dir === 'desc') {
        changeArrow('up', 1);
        if (+x.innerHTML < +y.innerHTML) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;
    } else {
      if (switchcount === 0 && dir === 'asc') {
        dir = 'desc';
        switching = true;
      }
    }
  }
}
