/* START TASK 1: Your code goes here */
let firstColumn = document.querySelectorAll('tr td:first-child');
firstColumn.forEach(i => i.addEventListener('click', changeColorToBlue));
let tds = document.querySelectorAll('td');
tds.forEach(i => i.addEventListener('click', changeColorToYellow));
let special = document.getElementById('special');
special.addEventListener('click', changeColorToGreen)

function changeColorToBlue() {
  let target = event.currentTarget;
  target.classList.remove('yellow');
  let tdsRow = target.parentElement;
  [...tdsRow.children].forEach(i => i.classList.toggle('blue'))
}
function changeColorToYellow() {
  let target = event.currentTarget;
  target.classList.toggle('yellow')
}
function changeColorToGreen() {
  special.classList.remove('yellow');
  special.classList.remove('blue');
  tds.forEach(i => {
    if (!(i.classList.contains('yellow') || i.classList.contains('blue'))) {
      i.classList.toggle('green')
    }
  })
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
  let input = document.querySelector('#task2 input');
  let success = document.getElementById('success');
  let btn = document.querySelector('#task2 button')
  input.addEventListener('input', updateValue);
  input.addEventListener('click', clearValue)
  btn.addEventListener('click', sendValue)
  function clearValue(){
    input.value = '';
  }
  function updateValue() {
    let value = event.target.value;
    let reg = /^\+380\d{9}$/;
    if (reg.test(value)) {
      success.innerHTML = '';
      input.style.borderColor = 'green';
      success.innerHTML = '';
      btn.removeAttribute('disabled');
    } else {
      success.innerHTML = '<p>The data doesn\'t follow format +380*********</p>'
      success.style.backgroundColor = '#DC143C';
    }
  }
  function sendValue() {
    btn.style.backgroundColor = 'green';
    input.value = '';
    success.style.backgroundColor = 'green';
    success.innerHTML = '<p>Data was successfully send</p>'
  }
/* END TASK 2 */

/* START TASK 3: Your code goes here */

let field = document.getElementById('court');
let ball = document.getElementById('ball');
let gateA = document.querySelector('.gateA');
let gateB = document.querySelector('.gateB');
let spanA = document.getElementById('scoreA');
let spanB = document.getElementById('scoreB');
let scoreA = 0;
let scoreB = 0;
function getClick() {
  let fieldCoords = this.getBoundingClientRect();
  let fieldBorderLeft = fieldCoords.left + field.clientLeft;
  let fieldBorderTop = fieldCoords.top + field.clientTop;
  let ballBorderLeft = event.clientX - fieldBorderLeft - ball.clientWidth / 2;
  let ballBorderTop = event.clientY - fieldBorderTop - ball.clientHeight / 2;
  if (ballBorderLeft < 0) {
    ballBorderLeft = 0;
  }
  if (ballBorderTop < 0) {
    ballBorderTop = 0;
  }
  if (ballBorderLeft + ball.clientWidth > field.clientWidth) {
    ballBorderLeft = field.clientWidth - ball.clientWidth;
  }
  if (ballBorderTop + ball.clientHeight > field.clientHeight) {
    ballBorderTop = field.clientHeight - ball.clientHeight;
  }
  ball.style.top = ballBorderTop + 'px';
  ball.style.left = ballBorderLeft + 'px';
}
function gollforB() {
  scoreA++
  spanA.innerHTML = `${scoreA}`;
}
function gollforA() {
  scoreB++
  spanB.innerHTML = `${scoreB}`;
}
gateA.addEventListener('click', gollforB);
gateB.addEventListener('click', gollforA);
field.addEventListener('click', getClick)
/* END TASK 3 */
