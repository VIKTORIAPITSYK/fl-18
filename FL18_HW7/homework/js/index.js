$(function() {
  let x = '';
  let curr;
  let expression;
  $('.calculator__btn').click(function() {
    curr = $(this).text();
    if (['+', '-', '*', '/'].some(item => item === x[x.length-1]) && $(this).hasClass('calcSighn')) {
      x = x.slice(0, x.length-1) + curr;
    } else if (curr === 'C') {
      $('.calculator__display').html('');
      $('.calculator__display').removeClass('redText');
      x = '';
    } else if (curr === '=') {
      if (x.match(/\/0/g)) {
        x = 'ERROR';
        $('.calculator__display').addClass('redText');
      } else {
        expression = x;
        x = evil(x);
        $('.calculator__display').html(x);
        $('.results__list').append(showResults(expression, x));
        console.log(x);
      }
    } else if (x !== 'ERROR') {
      x += curr;
    }
      $('.calculator__display').text(x);
  })
})

function evil(fn) {
  return new Function('return ' + fn)();
}

function showResults(exp, result) {
  return `<li class='results__list-item'>
  <span class='redCircle'></span>
  <span class='expression'>${exp} = ${result}</span>
  <span class='chriss'>✖</span></li>`
}

$(function () {
  $('body').on('click', '.redCircle', function() {
    $(this).toggleClass('redBackground');
  });
});

$(function () {
  $('body').on('click', '.chriss', function() {
    $(this).closest('li').remove();
  });
})

$(function () {
  $('.results').on('scroll', function() {
    console.log('Scroll Top:' + $('.results').scrollTop())
  });
});
