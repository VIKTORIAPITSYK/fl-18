const MIN_ARGUMENTS_NUMBER = 2;
const MAX_ARGUMENTS_NUMBER = 10;

const getBigestNumber = (...args) => {
  if (args.length < MIN_ARGUMENTS_NUMBER) {
    throw new Error('Not enough arguments');
  }
  if (args.length > MAX_ARGUMENTS_NUMBER) {
    throw new Error('Too many arguments');
  }

  if (args.some((el) => typeof el !== 'number')) {
    throw new Error('Wrong argument type');
  }

  return Math.max(...args);
};

module.exports = getBigestNumber;
