
const messages = document.getElementById('messages');
const form = document.getElementById('form');
const submit = document.getElementById('submit');
const ws = new WebSocket('ws://localhost:8080');
let username;

function setStatus(value) {
  const status = document.getElementById('status');
  status.innerHTML = value;
}
ws.onopen = () => {
  setStatus('ONLINE');
  while (!username) {
     username = prompt('What is your name?');
  }
};
ws.onmessage = (message) => {
  const messageData = JSON.parse(message.data);
  let li = document.createElement('li');
  li.classList.add('message__block');
  li.classList.add('message_left');
  const date = new Date();
  li.innerHTML = `
  <div>
  <span><i>${messageData.name}</i></span>
  <span class="newMessage">${messageData.message}</span>
  <span>${date.toLocaleString()}</span>
  </div>`
  messages.append(li);
}

const send = (event) => {
  event.preventDefault();
  let message = document.getElementById('message').value;
  ws.send(JSON.stringify({
    username, message
  }));
  let li = document.createElement('li');
  const date = new Date();
  li.classList.add('message__block');
  li.classList.add('message_right');
  li.innerHTML = `
  <div>
  <span><i>${name}</i></span>
  <span class="newMessage">${message}</span>
  <span>${date.toLocaleString()}</span>
  </div>`
  messages.append(li);
  document.getElementById('message').value = '';
  return false;
}

form.addEventListener('submit', send);
