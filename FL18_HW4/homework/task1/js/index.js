const usersList = document.getElementById('usersList');
let li, btn;
const url = 'https://jsonplaceholder.typicode.com/users';

fetch(url)
.then(function(response) {
  response.json()
  .then(function(json) {
    for (let i=0; i<json.length; i++) {
      li = document.createElement('li');
      li.innerHTML = `<span>${json[i].name}</span><input type='text' name='name'></input>
      <button name='update'>UPDATE</button><button id='id${json[i].id}' name='delete'>DELETE</button>`;
      usersList.append(li);
    }
    function deleteData(item) {
      return fetch(url + '/' + item, {
        method: 'delete'
      })
      .then(response => response.json());
    }
    let deletes = document.getElementsByName('delete');
    for (let btn of deletes) {
      btn.addEventListener('click', () => {
        console.log('Hello');
        let indetificator = event.target.id.replace(/id/, '');
        deleteData(indetificator);
        let parent = event.target.parentNode;
        parent.remove();
      })
    }
    let submits = document.getElementsByName('update');
    for (let sub of submits) {
      sub.addEventListener('click', () => {
        let closestInput = sub.previousElementSibling;
        let closestSpan = closestInput.previousElementSibling;
        closestSpan.innerText = `${closestInput.value}`;
      })
    }
  })
});
